DROP DATABASE IF EXISTS springbootjdbcthymeleaf;
CREATE DATABASE springbootjdbcthymeleaf;
USE springbootjdbcthymeleaf;
DROP TABLE IF EXISTS subscribers;
CREATE TABLE subscribers (
    subscriberId INTEGER PRIMARY KEY AUTO_INCREMENT,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE, 
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO subscribers (firstName, lastName, email) 
VALUES ("Melwic", "Guillaume", "melwic.g@gmail.com");