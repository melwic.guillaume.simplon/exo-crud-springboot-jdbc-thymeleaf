package com.co.simplon.springbootjdbcthymeleaf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.co.simplon.springbootjdbcthymeleaf.model.Subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO {
    
    private static final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";

    @Autowired 
    private DataSource dataSource;

    public SubscriberDAOImpl(DataSource dataSource) {
    }

    @Override
    public List<Subscriber> findAll() {
        Connection cnx;
        List<Subscriber> subscribers = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_SUBSCRIBERS);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                subscribers.add(new Subscriber(
                    result.getInt("subscriberId"), 
                    result.getString("firstName"), 
                    result.getString("lastName"), 
                    result.getString("email"), 
                    result.getTimestamp("createdAt")));
            }
            cnx.close();  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subscribers;
    }

}
