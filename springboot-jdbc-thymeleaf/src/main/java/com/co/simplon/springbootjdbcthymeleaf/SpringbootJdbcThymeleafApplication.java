package com.co.simplon.springbootjdbcthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJdbcThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJdbcThymeleafApplication.class, args);
	}

}
