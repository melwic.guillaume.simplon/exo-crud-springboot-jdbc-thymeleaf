package com.co.simplon.springbootjdbcthymeleaf.dao;

import java.util.List;

import com.co.simplon.springbootjdbcthymeleaf.model.Subscriber;


public interface SubscriberDAO {
    public List<Subscriber> findAll();
}
